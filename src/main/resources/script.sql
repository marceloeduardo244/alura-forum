--
-- Name: curso; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE public.curso (
    id bigint NOT NULL,
    categoria character varying(255),
    nome character varying(255)
);

--
-- Name: curso_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--
ALTER TABLE
    public.curso
ALTER COLUMN
    id
ADD
    GENERATED BY DEFAULT AS IDENTITY (
        SEQUENCE NAME public.curso_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1
    );

--
-- Name: resposta; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE public.resposta (
    id bigint NOT NULL,
    data_criacao timestamp without time zone,
    mensagem character varying(255),
    solucao boolean,
    autor_id bigint,
    topico_id bigint
);

--
-- Name: resposta_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--
ALTER TABLE
    public.resposta
ALTER COLUMN
    id
ADD
    GENERATED BY DEFAULT AS IDENTITY (
        SEQUENCE NAME public.resposta_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1
    );

--
-- Name: topico; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE public.topico (
    id bigint NOT NULL,
    data_criacao timestamp without time zone,
    mensagem character varying(255),
    status character varying(255),
    titulo character varying(255),
    autor_id bigint,
    curso_id bigint
);

--
-- Name: topico_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--
ALTER TABLE
    public.topico
ALTER COLUMN
    id
ADD
    GENERATED BY DEFAULT AS IDENTITY (
        SEQUENCE NAME public.topico_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1
    );

--
-- Name: usuario; Type: TABLE; Schema: public; Owner: -
--
CREATE TABLE public.usuario (
    id bigint NOT NULL,
    email character varying(255),
    nome character varying(255),
    senha character varying(255)
);

--
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--
ALTER TABLE
    public.usuario
ALTER COLUMN
    id
ADD
    GENERATED BY DEFAULT AS IDENTITY (
        SEQUENCE NAME public.usuario_id_seq START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE CACHE 1
    );

--
-- Name: curso curso_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--
ALTER TABLE
    ONLY public.curso
ADD
    CONSTRAINT curso_pkey PRIMARY KEY (id);

--
-- Name: resposta resposta_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--
ALTER TABLE
    ONLY public.resposta
ADD
    CONSTRAINT resposta_pkey PRIMARY KEY (id);

--
-- Name: topico topico_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--
ALTER TABLE
    ONLY public.topico
ADD
    CONSTRAINT topico_pkey PRIMARY KEY (id);

--
-- Name: usuario usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--
ALTER TABLE
    ONLY public.usuario
ADD
    CONSTRAINT usuario_pkey PRIMARY KEY (id);

--
-- Name: resposta fk9999kvnmdq63ah7imctrl06r7; Type: FK CONSTRAINT; Schema: public; Owner: -
--
ALTER TABLE
    ONLY public.resposta
ADD
    CONSTRAINT fk9999kvnmdq63ah7imctrl06r7 FOREIGN KEY (autor_id) REFERENCES public.usuario(id);

--
-- Name: topico fkcaaogjo0ynd54updie6kdpxd1; Type: FK CONSTRAINT; Schema: public; Owner: -
--
ALTER TABLE
    ONLY public.topico
ADD
    CONSTRAINT fkcaaogjo0ynd54updie6kdpxd1 FOREIGN KEY (curso_id) REFERENCES public.curso(id);

--
-- Name: resposta fkltuv9rkfjtlmn8b0rb3wdbjsv; Type: FK CONSTRAINT; Schema: public; Owner: -
--
ALTER TABLE
    ONLY public.resposta
ADD
    CONSTRAINT fkltuv9rkfjtlmn8b0rb3wdbjsv FOREIGN KEY (topico_id) REFERENCES public.topico(id);

--
-- Name: topico fksk04hscorwqdymnafg8882v64; Type: FK CONSTRAINT; Schema: public; Owner: -
--
ALTER TABLE
    ONLY public.topico
ADD
    CONSTRAINT fksk04hscorwqdymnafg8882v64 FOREIGN KEY (autor_id) REFERENCES public.usuario(id);

--
-- PostgreSQL database dump complete
--