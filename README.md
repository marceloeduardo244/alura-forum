# alura-forum

Projeto desenvolvido para ser uma API RESTful, simulando um forúm.

## Acessando a aplicação
- Usuario nível/role aluno: 
    - {
	    "email": "aluno@email.com",
	    "senha": "123456"
      }
- Usuário nível/role moderador:
    - {
	    "email": "moderador@email.com",
	    "senha": "123456"
      }

### Testando a aplicação no ambiente de staging

- Deploy no heroku do ambiente
    - https://alura-forum-2022-env-staging.herokuapp.com/
- Documentação da API com Swagger
    - https://alura-forum-2022-env-staging.herokuapp.com/swagger-ui.html

#### Testando a aplicação no ambiente de production

- Deploy no heroku do ambiente
    - https://alura-forum-2022-env-prod.herokuapp.com/
- Documentação da API com Swagger
    - https://alura-forum-2022-env-prod.herokuapp.com/swagger-ui.html

##### Rotas da aplicação

- POST /auth

- GET /topicos

- GET /topicos/{id}

- DELETE /topicos/{id} (Token)
    - somente moderador

- POST /topicos (Token)
    -   {
	        "titulo": "Duvida duvidosa",
	        "mensagem": "uma mensagem",
	        "nomeCurso": "Spring Boot"
        }

- PUT /topicos/{id} (Token)
    -   {
	        "titulo": "Duvida duvidosa",
	        "mensagem": "outra mensagem",
        }
